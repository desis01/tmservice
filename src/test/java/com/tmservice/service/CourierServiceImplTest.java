package com.tmservice.service;


import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.ProtocolVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicStatusLine;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.tmservice.entity.Company;
import com.tmservice.entity.Courier;
import com.tmservice.entity.CourierRate;
import com.tmservice.exception.InputNullException;
import com.tmservice.exception.RunningNoEmptyException;
import com.tmservice.repository.ICompanyRepository;
import com.tmservice.repository.ICourierRateRepository;
import com.tmservice.repository.ICourierRepository;
import com.tmservice.service.impl.CourierServiceImpl;
import com.tmservice.vo.InputObject;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class CourierServiceImplTest {

	@Mock
	CourierServiceImpl courierServiceImpl;
	
	@Mock
    ICourierRepository courierRepository;
	
	@Mock
    private ICompanyRepository companyRepository;
	
	@Mock
    private HttpResponse httpResponse;
	
	@Mock
    private HttpEntity httpEntity;
	
	@Mock
    private ICourierRateRepository courierRateRepository;

	@Test
	public void onCheckingInputNullSelectedTypeOneTest() throws InputNullException {
		InputObject inputObj = new InputObject();
		inputObj.setSenderCountry("MY");
		inputObj.setItemHeight(BigDecimal.valueOf(50));
		inputObj.setItemLength(BigDecimal.valueOf(20));
		inputObj.setItemWeight(BigDecimal.valueOf(1));
		inputObj.setItemWidth(BigDecimal.valueOf(50));
		inputObj.setReceiverCountry("MY");
		inputObj.setReceiverPoscode("43500");
		inputObj.setReceiverState("Selangor");
		inputObj.setSenderPoscode("08000");
		inputObj.setSenderState("Kedah");

		courierServiceImpl.onCheckingInputNull(1, inputObj);

	}

	@Test
	public void onCheckingInputNullSelectedTypeTwoTest() throws InputNullException {
		InputObject inputObj = new InputObject();
		inputObj.setSenderCountry("MY");
		inputObj.setItemHeight(BigDecimal.valueOf(50));
		inputObj.setItemLength(BigDecimal.valueOf(20));
		inputObj.setItemWeight(BigDecimal.valueOf(1));
		inputObj.setItemWidth(BigDecimal.valueOf(50));
		inputObj.setReceiverCountry("MY");
		inputObj.setReceiverPoscode("43500");
		inputObj.setReceiverState("Selangor");
		inputObj.setSenderPoscode("08000");
		inputObj.setSenderState("Kedah");

		courierServiceImpl.onCheckingInputNull(2, inputObj);

	}
	
	@Test
	public void insertTableCourierOneTest() throws RunningNoEmptyException {
		Mockito.lenient().when(courierServiceImpl.onGenerateBatchNo()).thenReturn("ABC123");
		
		InputObject inputObj = new InputObject();
        inputObj.setSenderCountry("MY");
        inputObj.setItemHeight(BigDecimal.valueOf(50));
        inputObj.setItemLength(BigDecimal.valueOf(20));
        inputObj.setItemWeight(BigDecimal.valueOf(1));
        inputObj.setItemWidth(BigDecimal.valueOf(50));
        inputObj.setReceiverCountry("MY");
        inputObj.setReceiverPoscode("43500");
        inputObj.setReceiverState("Selangor");
        inputObj.setSenderPoscode("08000");
        inputObj.setSenderState("Kedah");
        
        Courier expectedCourier = new Courier();
        expectedCourier.setBatchNo("ABC123");
        expectedCourier.setSelectedType(1);
        expectedCourier.setSenderCountry("MY");
        expectedCourier.setSenderState("Kedah");
        expectedCourier.setSenderPoscode("08000");
        expectedCourier.setReceiverCountry("MY");
        expectedCourier.setReceiverState("Selangor");
        expectedCourier.setReceiverPoscode("43500");
        expectedCourier.setItemHeight(BigDecimal.valueOf(50));
        expectedCourier.setItemLength(BigDecimal.valueOf(20));
        expectedCourier.setItemWeight(BigDecimal.valueOf(1));
        expectedCourier.setItemWidth(BigDecimal.valueOf(50));
        
        Mockito.lenient().when(courierRepository.save(any(Courier.class))).thenReturn(expectedCourier);
        
        Long id = courierServiceImpl.insertTableCourier(1, inputObj);
        
        assertEquals(0, id);
        verify(courierRepository, times(0)).save(any(Courier.class));
	}
	
	@Test
	public void insertTableCourierTwoTest() throws RunningNoEmptyException {
		Mockito.lenient().when(courierServiceImpl.onGenerateBatchNo()).thenReturn("ABC123");
		
		InputObject inputObj = new InputObject();
        inputObj.setSenderCountry("MY");
        inputObj.setItemHeight(BigDecimal.valueOf(50));
        inputObj.setItemLength(BigDecimal.valueOf(20));
        inputObj.setItemWeight(BigDecimal.valueOf(1));
        inputObj.setItemWidth(BigDecimal.valueOf(50));
        inputObj.setReceiverCountry("MY");
        inputObj.setReceiverPoscode("43500");
        inputObj.setReceiverState("Selangor");
        inputObj.setSenderPoscode("08000");
        inputObj.setSenderState("Kedah");
        
        Courier expectedCourier = new Courier();
        expectedCourier.setBatchNo("ABC123");
        expectedCourier.setSelectedType(1);
        expectedCourier.setSenderCountry("MY");
        expectedCourier.setSenderState("Kedah");
        expectedCourier.setSenderPoscode("08000");
        expectedCourier.setReceiverCountry("MY");
        expectedCourier.setReceiverState("Selangor");
        expectedCourier.setReceiverPoscode("43500");
        expectedCourier.setItemHeight(BigDecimal.valueOf(50));
        expectedCourier.setItemLength(BigDecimal.valueOf(20));
        expectedCourier.setItemWeight(BigDecimal.valueOf(1));
        expectedCourier.setItemWidth(BigDecimal.valueOf(50));
        
        Mockito.lenient().when(courierRepository.save(any(Courier.class))).thenReturn(expectedCourier);
        
        Long id = courierServiceImpl.insertTableCourier(2, inputObj);
        
        assertEquals(0, id);
        verify(courierRepository, times(0)).save(any(Courier.class));
	}
	
	@Test
	public void onCallApiSelectedOne() throws RunningNoEmptyException {
		Integer selectedType = 1;
        InputObject inputObj = new InputObject();
        inputObj.setSenderCountry("MY");
        inputObj.setItemHeight(BigDecimal.valueOf(50));
        inputObj.setItemLength(BigDecimal.valueOf(20));
        inputObj.setItemWeight(BigDecimal.valueOf(1));
        inputObj.setItemWidth(BigDecimal.valueOf(50));
        inputObj.setReceiverCountry("MY");
        inputObj.setReceiverPoscode("43500");
        inputObj.setReceiverState("Selangor");
        inputObj.setSenderPoscode("08000");
        inputObj.setSenderState("Kedah");
        
        Long courierId = 1L;
        
        Company citylink = new Company();
        citylink.setCompanyName("CITYLINK");
        citylink.setParcelDocument("PARCEL");
        
        Company dhl = new Company();
        dhl.setCompanyName("DHL");
        dhl.setParcelDocument("PARCEL");
        
        List<Company> companyList = Arrays.asList(citylink, dhl);
        
        Mockito.lenient().when(courierServiceImpl.onGenerateBatchNo()).thenReturn("ABC123");
        
        courierServiceImpl.onCallApi(selectedType, inputObj, courierId);
        
        verify(courierServiceImpl, times(0)).onGenerateCityLink(selectedType, inputObj, courierId, citylink);
        verify(courierServiceImpl, times(0)).onGenerateDhl(selectedType, inputObj, courierId, dhl);
	}
	
	@Test
	public void onCallApiSelectedTwo() throws RunningNoEmptyException {
		Integer selectedType = 2;
        InputObject inputObj = new InputObject();
        inputObj.setSenderCountry("MY");
        inputObj.setItemHeight(BigDecimal.valueOf(50));
        inputObj.setItemLength(BigDecimal.valueOf(20));
        inputObj.setItemWeight(BigDecimal.valueOf(1));
        inputObj.setItemWidth(BigDecimal.valueOf(50));
        inputObj.setReceiverCountry("MY");
        inputObj.setReceiverPoscode("43500");
        inputObj.setReceiverState("Selangor");
        inputObj.setSenderPoscode("08000");
        inputObj.setSenderState("Kedah");
        
        Long courierId = 1L;
        
        Company citylink = new Company();
        citylink.setCompanyName("CITYLINK");
        citylink.setParcelDocument("PARCEL");
        
        Company dhl = new Company();
        dhl.setCompanyName("DHL");
        dhl.setParcelDocument("PARCEL");
        
        List<Company> companyList = Arrays.asList(citylink, dhl);
        
        Mockito.lenient().when(courierServiceImpl.onGenerateBatchNo()).thenReturn("ABC123");
        
        courierServiceImpl.onCallApi(selectedType, inputObj, courierId);
        
        verify(courierServiceImpl, times(0)).onGenerateCityLink(selectedType, inputObj, courierId, citylink);
        verify(courierServiceImpl, times(0)).onGenerateDhl(selectedType, inputObj, courierId, dhl);
	}
	
	@Test
	public void onGenerateDhl() throws ParseException, IOException {
		Integer selectedType = 1;
        InputObject inputObj = new InputObject();
        inputObj.setSenderCountry("MY");
        inputObj.setItemHeight(BigDecimal.valueOf(50));
        inputObj.setItemLength(BigDecimal.valueOf(20));
        inputObj.setItemWeight(BigDecimal.valueOf(1));
        inputObj.setItemWidth(BigDecimal.valueOf(50));
        inputObj.setReceiverCountry("MY");
        inputObj.setReceiverPoscode("43500");
        inputObj.setReceiverState("Selangor");
        inputObj.setSenderPoscode("08000");
        inputObj.setSenderState("Kedah");
        
        Long courierId = 1L;
        
        Company dhl = new Company();
        dhl.setCompanyName("DHL");
        dhl.setParcelDocument("PARCEL");
        dhl.setUrlPath("https://cj-gaq.dhl.com/api/quote");
        
        Mockito.lenient().when(httpResponse.getStatusLine()).thenReturn(new BasicStatusLine(new ProtocolVersion("HTTP", 1, 1), 200, ""));
        Mockito.lenient().when(httpResponse.getEntity()).thenReturn(httpEntity);
        
        Mockito.lenient().when(httpResponse.getEntity()).thenReturn(httpEntity);
        
        courierServiceImpl.onGenerateDhl(selectedType, inputObj, courierId, dhl);
        
        verify(courierRateRepository, times(0)).save(any(CourierRate.class));
        
	}
	
	@Test
	public void onGenerateCityLink() throws ClientProtocolException, IOException {
		Integer selectedType = 1;
        InputObject inputObj = new InputObject();
        inputObj.setSenderCountry("MY");
        inputObj.setItemHeight(BigDecimal.valueOf(50));
        inputObj.setItemLength(BigDecimal.valueOf(20));
        inputObj.setItemWeight(BigDecimal.valueOf(1));
        inputObj.setItemWidth(BigDecimal.valueOf(50));
        inputObj.setReceiverCountry("MY");
        inputObj.setReceiverPoscode("43500");
        inputObj.setReceiverState("Selangor");
        inputObj.setSenderPoscode("08000");
        inputObj.setSenderState("Kedah");
        
        Company dhl = new Company();
        dhl.setCompanyName("DHL");
        dhl.setParcelDocument("PARCEL");
        dhl.setUrlPath("https://cj-gaq.dhl.com/api/quote");
        
        Long courierId = 1L;
        
        String expectedRequestBody = "origin_country=Malaysia&origin_state=Selangor&origin_postcode=47500"
                + "&destination_country=Singapore&destination_state=Singapore&destination_postcode=659480"
                + "&selected_type=2&document_weight=0.5";
        String expectedResponseBody = "{\"req\": {\"data\": {\"rate\": 10.0}}}";
        
        HttpClient httpClient = Mockito.mock(HttpClient.class);
        HttpPost httpPost = Mockito.mock(HttpPost.class);
        
        Mockito.lenient().when(httpClient.execute(httpPost)).thenReturn(httpResponse);
        Mockito.lenient().when(httpResponse.getEntity()).thenReturn(new StringEntity(expectedResponseBody));
        
        Mockito.lenient().when(courierRateRepository.save(Mockito.any(CourierRate.class)))
        .thenReturn(Mockito.mock(CourierRate.class));
        
        courierServiceImpl.setHttpClient(httpClient);
        
        courierServiceImpl.onGenerateCityLink(selectedType, inputObj, courierId, dhl);


	}

}
