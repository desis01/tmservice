package com.tmservice.controller;

import org.apache.http.client.HttpClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.tmservice.entity.CourierRate;
import com.tmservice.exception.InputNullException;
import com.tmservice.exception.RunningNoEmptyException;
import com.tmservice.repository.ICompanyRepository;
import com.tmservice.repository.ICourierRepository;
import com.tmservice.repository.IRunningNoRepository;
import com.tmservice.service.ICourierService;
import com.tmservice.service.impl.CourierServiceImpl;
import com.tmservice.vo.CourierRateObject;
import com.tmservice.vo.InputObject;
import com.tmservice.vo.OutputObject;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ShippingControllerTest {
	@InjectMocks
	private ShippingController shippingController;

	@Mock
	private ICourierService courierService;

	@InjectMocks
	private CourierServiceImpl courierServiceImpl;

	@Mock
	private IRunningNoRepository runningNoRepository;

	@Mock
	private ICourierRepository courierRepository;

	@Mock
	private ICompanyRepository companyRepository;
	
	private CourierServiceImpl myClass;
    
    @Mock
    private HttpClient httpClientMock;
    

	@Test
	public void testGetHelloWorld() {
		// Set up the X-API-KEY header
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-API-Key", "AbCdEfGhIjKlMnOpQrStUvWxYz0123456789");

		// Call the controller method
		ResponseEntity<?> responseEntity = shippingController.getHelloWorld();

		// Assert that the response is correct
		assertEquals(200, responseEntity.getStatusCodeValue());
		assertEquals("Hello World", responseEntity.getBody());
	}
	
	@Test
	public void testCheckingCourierRateSeletedTypeOneCityBank() throws InputNullException, RunningNoEmptyException {
		InputObject inputObj = new InputObject();
		inputObj.setSenderCountry("MY");
		inputObj.setItemHeight(BigDecimal.valueOf(50));
		inputObj.setItemLength(BigDecimal.valueOf(20));
		inputObj.setItemWeight(BigDecimal.valueOf(1));
		inputObj.setItemWidth(BigDecimal.valueOf(50));
		inputObj.setReceiverCountry("MY");
		inputObj.setReceiverPoscode("43500");
		inputObj.setReceiverState("Selangor");
		inputObj.setSenderPoscode("08000");
		inputObj.setSenderState("Kedah");
		
		CourierRate courierRate = new CourierRate();
		courierRate.setCourier("CITYLINK");
		courierRate.setRate(new BigDecimal(10.0));
		
		List<CourierRateObject> courierRateObjects = Collections.singletonList(new CourierRateObject(courierRate));
		OutputObject expectedOutput = new OutputObject();
		expectedOutput.setData(courierRateObjects);
		
		Mockito.lenient().when(courierService.calculateRateCourier(1, inputObj)).thenReturn(expectedOutput);
		
		ResponseEntity<OutputObject> response = shippingController.checkingCourierRate(1, inputObj);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
	    assertEquals(expectedOutput, response.getBody());
	}
	
	@Test
	public void testCheckingCourierRateSeletedTypeOneDHL() throws InputNullException, RunningNoEmptyException {
		InputObject inputObj = new InputObject();
		inputObj.setSenderCountry("MY");
		inputObj.setItemHeight(BigDecimal.valueOf(50));
		inputObj.setItemLength(BigDecimal.valueOf(20));
		inputObj.setItemWeight(BigDecimal.valueOf(1));
		inputObj.setItemWidth(BigDecimal.valueOf(50));
		inputObj.setReceiverCountry("MY");
		inputObj.setReceiverPoscode("43500");
		inputObj.setReceiverState("Selangor");
		inputObj.setSenderPoscode("08000");
		inputObj.setSenderState("Kedah");
		
		CourierRate courierRate = new CourierRate();
		courierRate.setCourier("DHL");
		courierRate.setRate(new BigDecimal(10.0));
		
		List<CourierRateObject> courierRateObjects = Collections.singletonList(new CourierRateObject(courierRate));
		OutputObject expectedOutput = new OutputObject();
		expectedOutput.setData(courierRateObjects);
		
		Mockito.lenient().when(courierService.calculateRateCourier(1, inputObj)).thenReturn(expectedOutput);
		
		ResponseEntity<OutputObject> response = shippingController.checkingCourierRate(1, inputObj);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
	    assertEquals(expectedOutput, response.getBody());
	}
	
	@Test
	public void testCheckingCourierRateSeletedTypeTwoCityBank() throws InputNullException, RunningNoEmptyException {
		InputObject inputObj = new InputObject();
		inputObj.setSenderCountry("MY");
		inputObj.setItemWeight(BigDecimal.valueOf(1));
		inputObj.setReceiverCountry("MY");
		inputObj.setReceiverPoscode("43500");
		inputObj.setReceiverState("Selangor");
		inputObj.setSenderPoscode("08000");
		inputObj.setSenderState("Kedah");
		
		CourierRate courierRate = new CourierRate();
		courierRate.setCourier("CITYLINK");
		courierRate.setRate(new BigDecimal(10.0));
		
		List<CourierRateObject> courierRateObjects = Collections.singletonList(new CourierRateObject(courierRate));
		OutputObject expectedOutput = new OutputObject();
		expectedOutput.setData(courierRateObjects);
		
		Mockito.lenient().when(courierService.calculateRateCourier(2, inputObj)).thenReturn(expectedOutput);
		
		ResponseEntity<OutputObject> response = shippingController.checkingCourierRate(2, inputObj);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
	    assertEquals(expectedOutput, response.getBody());
	}
	

}