package com.tmservice.conf;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static com.tmservice.conf.ConfConfigTest.API_KEY;
import static com.tmservice.conf.ConfConfigTest.API_KEY_HEADER;

public class ApiKeyFilterTest {

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    private ApiKeyFilter apiKeyFilter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        apiKeyFilter = new ApiKeyFilter();
    }

    @Test
    public void testValidApiKey() throws Exception {
        when(request.getHeader(API_KEY_HEADER)).thenReturn(API_KEY);

        boolean result = apiKeyFilter.preHandle(request, response, null);

        assertTrue(result);
        verify(response, never()).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }

    @Test
    public void testMissingApiKey() throws Exception {
        when(request.getHeader(API_KEY_HEADER)).thenReturn(null);

        boolean result = apiKeyFilter.preHandle(request, response, null);

        assertFalse(result);
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }

    @Test
    public void testInvalidApiKey() throws Exception {
        when(request.getHeader(API_KEY_HEADER)).thenReturn("invalid-key");

        boolean result = apiKeyFilter.preHandle(request, response, null);

        assertFalse(result);
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }

}
