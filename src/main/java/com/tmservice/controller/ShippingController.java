package com.tmservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tmservice.exception.InputNullException;
import com.tmservice.exception.RunningNoEmptyException;
import com.tmservice.service.ICourierService;
import com.tmservice.vo.InputObject;
import com.tmservice.vo.OutputObject;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping(value = {"/shipping"})
@Api(value = "Courier System")
public class ShippingController {
	
	@Autowired private ICourierService courierService;

	@ApiOperation(value = "Get Starter")
	@GetMapping("/helloworld")
	public ResponseEntity<?> getHelloWorld(){
		return ResponseEntity.ok("Hello World");
	}
	
	@ApiOperation(value = "Checking Rate Courier System")
	@PostMapping("/check-rate")
	public ResponseEntity<OutputObject> checkingCourierRate(@RequestParam("selectedType") Integer selectedType, 
												 @RequestBody InputObject inputObj) throws InputNullException, RunningNoEmptyException{
		return ResponseEntity.ok(courierService.calculateRateCourier(selectedType, inputObj));
	}
}
