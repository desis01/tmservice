package com.tmservice.exception;

import java.util.Objects;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.tmservice.conf.HttpResponse;


@RestControllerAdvice
public class ExceptionHandling implements ErrorController{
	
	private static final String THIS_PAGE_WAS_NOT_FOUND = "This page was not found";
	private static final String METHOD_IS_NOT_ALLOWED = "This request method is not allowed on this endpoint. Please send a '%s' request";
	
	@ExceptionHandler(RunningNoEmptyException.class)
	public ResponseEntity<HttpResponse> runningNoEmptyException(RunningNoEmptyException exception){
		return createHttpResponse(HttpStatus.BAD_REQUEST,exception.getMessage());
	} 
	
	@ExceptionHandler(InputNullException.class)
	public ResponseEntity<HttpResponse> inputNullException(InputNullException exception){
		return createHttpResponse(HttpStatus.BAD_REQUEST,exception.getMessage());
	} 
	
	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	public ResponseEntity<HttpResponse> methodNotSupportedException(HttpRequestMethodNotSupportedException exception){
		HttpMethod supportedMethod = Objects.requireNonNull(exception.getSupportedHttpMethods()).iterator().next();
		return createHttpResponse(HttpStatus.METHOD_NOT_ALLOWED,String.format(METHOD_IS_NOT_ALLOWED, supportedMethod));
	}
	
	@ExceptionHandler(NoHandlerFoundException.class)
	public ResponseEntity<HttpResponse> methodNotSupportedException(NoHandlerFoundException e){
		return createHttpResponse(HttpStatus.BAD_REQUEST,THIS_PAGE_WAS_NOT_FOUND);
	}
	
	private ResponseEntity<HttpResponse> createHttpResponse(HttpStatus httpStatus, String message){
		return new ResponseEntity<>(new HttpResponse(httpStatus.value(),httpStatus,httpStatus.getReasonPhrase().toUpperCase()
				,message),httpStatus);
	}
	
	@Override
    public String getErrorPath() {
        return "error";
    }

}
