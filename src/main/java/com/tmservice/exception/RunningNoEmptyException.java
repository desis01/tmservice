package com.tmservice.exception;

public class RunningNoEmptyException extends Exception{
	public RunningNoEmptyException(String message) {
		super(message);
	}
}
