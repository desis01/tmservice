package com.tmservice.exception;

public class InputNullException extends Exception {
	public InputNullException(String message) {
		super(message);
	}
}
