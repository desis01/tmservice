package com.tmservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tmservice.entity.Company;

public interface ICompanyRepository extends JpaRepository<Company, Long>{

}
