package com.tmservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tmservice.entity.CourierRate;

public interface ICourierRateRepository extends JpaRepository<CourierRate, Long> {

}
