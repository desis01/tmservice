package com.tmservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tmservice.entity.RunningNo;

public interface IRunningNoRepository extends JpaRepository<RunningNo, Long>{

}
