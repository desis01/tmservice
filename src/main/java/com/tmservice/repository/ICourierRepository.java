package com.tmservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tmservice.entity.Courier;

public interface ICourierRepository extends JpaRepository<Courier, Long>{

}
