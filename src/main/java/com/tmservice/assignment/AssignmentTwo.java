package com.tmservice.assignment;

import java.util.stream.IntStream;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AssignmentTwo {

	@Bean
	public void generateSourceCode() {
		int[] arr = {1, 2, 4, 7, 11, 15};
		
		System.out.println("Output ========>"+sumSequences(arr,15));
	}
	
	private boolean sumSequences(int[] arr, int targetSum) {
		return IntStream.range(0, arr.length)
                .anyMatch(i -> IntStream.range(i + 1, arr.length)
                                     .anyMatch(j -> arr[i] + arr[j] == targetSum));
	}
	
	
	
}
