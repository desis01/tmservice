package com.tmservice.service;

import com.tmservice.exception.InputNullException;
import com.tmservice.exception.RunningNoEmptyException;
import com.tmservice.vo.InputObject;
import com.tmservice.vo.OutputObject;

public interface ICourierService {
	
	OutputObject calculateRateCourier(Integer selectedType, InputObject inputObj) throws InputNullException, RunningNoEmptyException;

}
