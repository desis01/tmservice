package com.tmservice.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmservice.entity.Company;
import com.tmservice.entity.Courier;
import com.tmservice.entity.CourierRate;
import com.tmservice.entity.RunningNo;
import com.tmservice.exception.InputNullException;
import com.tmservice.exception.RunningNoEmptyException;
import com.tmservice.repository.ICompanyRepository;
import com.tmservice.repository.ICourierRateRepository;
import com.tmservice.repository.ICourierRepository;
import com.tmservice.repository.IRunningNoRepository;
import com.tmservice.service.ICourierService;
import com.tmservice.vo.CourierRateObject;
import com.tmservice.vo.InputObject;
import com.tmservice.vo.OutputObject;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import com.fasterxml.jackson.databind.JsonNode;

import java.math.BigDecimal;
import java.net.URI;
import java.nio.charset.StandardCharsets;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CourierServiceImpl implements ICourierService {

	@Autowired
	private ICourierRepository courierRepository;
	@Autowired
	private IRunningNoRepository runningNoRepository;
	@Autowired
	private ICompanyRepository companyRepository;
	@Autowired
	private ICourierRateRepository courierRateRepository;
	

	@Override
	public OutputObject calculateRateCourier(Integer selectedType, InputObject inputObj)
			throws InputNullException, RunningNoEmptyException {
		// TODO Auto-generated method stub
		Long courierId = 0l;
        List<Courier> courList = courierRepository.findAll(Example.of(this.onMapInputObjectToCourier(inputObj,selectedType)));
        if(courList.size() == 0) {
        	// check for handling error
    		this.onCheckingInputNull(selectedType, inputObj);

    		// insert table courier that have attribute batchNo, InputObject, createdDate,
    		// deleted, selectedType
    		courierId = this.insertTableCourier(selectedType, inputObj);
    		log.info(courierId.toString());
//    		
    		// call api based on selected_type... if selected_type = 1 call only dhl &
    		// citylink then selected_type = 2 call only citylink
    		this.onCallApi(selectedType, inputObj, courierId);
        }else {
        	courierId = courList.get(0).getCourierId();
        }
        
		

		return this.getListDisplay(courierId);
	}
	
	public Courier onMapInputObjectToCourier(InputObject inputObj,Integer selectedType) {
		Courier courierMap = new Courier();
		courierMap.setSelectedType(selectedType);
		courierMap.setSenderCountry(inputObj.getSenderCountry());
		courierMap.setSenderState(inputObj.getSenderState());
		courierMap.setSenderPoscode(inputObj.getSenderPoscode());
		courierMap.setReceiverCountry(inputObj.getReceiverCountry());
		courierMap.setReceiverState(inputObj.getReceiverState());
		courierMap.setReceiverPoscode(inputObj.getReceiverPoscode());
		courierMap.setItemLength(inputObj.getItemLength());
		courierMap.setItemHeight(inputObj.getItemHeight());
		courierMap.setItemWeight(inputObj.getItemWeight());
		courierMap.setItemWidth(inputObj.getItemWidth());
		
		return courierMap;
	}

	private OutputObject getListDisplay(Long courierId) {
		OutputObject outIo = new OutputObject();

		CourierRate cr = new CourierRate();
		cr.setCourierId(courierId);

		List<CourierRate> crList = courierRateRepository.findAll(Example.of(cr));

		List<CourierRateObject> courierRateList = crList.stream().map(CourierRateObject::new)
				.collect(Collectors.toList());
		outIo.setData(courierRateList);

		return outIo;
	}

	public void onCallApi(Integer selectedType, InputObject inputObj, Long courierId) {

		List<Company> companyList = companyRepository.findAll();

		if (selectedType.equals(1)) {
			// call dhl and citylink
			for (Company com : companyList) {
				if (com.getParcelDocument().contains("PARCEL")) {
					if (com.getCompanyName().equals("CITYLINK")) {
						this.onGenerateCityLink(selectedType, inputObj, courierId, com);
					} else if (com.getCompanyName().equals("DHL")) {
						this.onGenerateDhl(selectedType, inputObj, courierId, com);
					}

				}
			}

		} else if (selectedType.equals(2)) {
			// call citylink
			for (Company com : companyList) {
				if (com.getParcelDocument().contains("DOCUMENT")) {
					if (com.getCompanyName().equals("CITYLINK")) {
						this.onGenerateCityLink(selectedType, inputObj, courierId, com);
					}

				}
			}

		}
	}

	public void onGenerateDhl(Integer selectedType, InputObject inputObj, Long courierId, Company compObj) {
		HttpClient client = HttpClients.createDefault();

		try {
			URIBuilder builder = new URIBuilder(compObj.getUrlPath());
			builder.setParameter("destinationCity", inputObj.getReceiverState()).setParameter("destinationCountry", inputObj.getReceiverCountry())
					.setParameter("destinationZip", inputObj.getReceiverPoscode()).setParameter("originCity", inputObj.getSenderState())
					.setParameter("originCountry", inputObj.getSenderCountry()).setParameter("originZip", inputObj.getSenderPoscode())
					.setParameter("items(0).weight", inputObj.getItemWeight().toString()).setParameter("items(0).height", inputObj.getItemHeight().toString())
					.setParameter("items(0).length", inputObj.getItemLength().toString()).setParameter("items(0).width", inputObj.getItemWidth().toString())
					.setParameter("items(0).quantity", "1");

			URI uri = builder.build();
			HttpGet request = new HttpGet(uri);

			HttpResponse response = client.execute(request);
			String jsonResponse = EntityUtils.toString(response.getEntity());

			log.info("Status code: " + response.getStatusLine().getStatusCode());

			if (response.getStatusLine().getStatusCode() == 200) {
				ObjectMapper mapper = new ObjectMapper();
				JsonNode root = mapper.readTree(jsonResponse);

				JsonNode dataNode = root.get("offers").get(0).get("price");

				Double rate = dataNode.get("billingAmount").asDouble();

				CourierRate courierRate = new CourierRate();
				courierRate.setCreatedDate(new Date());
				courierRate.setCourier(compObj.getCompanyName());
				courierRate.setRate(BigDecimal.valueOf(rate));
				courierRate.setCourierId(courierId);

				courierRateRepository.save(courierRate);
				log.info("Successfully insert to courier rate");
			} else {
				log.error("Error status code :" + response.getStatusLine().getStatusCode());
			}

		} catch (Exception ex) {
			log.error("Error ===>" + ex.getMessage());
		}

	}

	public void onGenerateCityLink(Integer selectedType, InputObject inputObj, Long courierId, Company compObj) {

		HttpClient httpClient = HttpClientBuilder.create().build();

		HttpPost httpPost = new HttpPost(compObj.getUrlPath());

		httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");

		String body = "";
		if (selectedType.equals(2)) {
			body = "origin_country=" + inputObj.getSenderCountry() + "&origin_state=" + inputObj.getSenderState()
					+ "&origin_postcode=" + inputObj.getSenderPoscode() + "" + "&destination_country="
					+ inputObj.getReceiverCountry() + "&destination_state=" + inputObj.getReceiverState()
					+ "&destination_postcode=" + inputObj.getReceiverPoscode() + "" + "&selected_type=" + selectedType
					+ "&document_weight=" + inputObj.getItemWeight();
		} else if (selectedType.equals(1)) {
			body = "origin_country=" + inputObj.getSenderCountry() + "&origin_state=" + inputObj.getSenderState()
					+ "&origin_postcode=" + inputObj.getSenderPoscode() + "" + "&destination_country="
					+ inputObj.getReceiverCountry() + "&destination_state=" + inputObj.getReceiverState()
					+ "&destination_postcode=" + inputObj.getReceiverPoscode() + "" + "&selected_type=" + selectedType
					+ "&parcel_weight=" + inputObj.getItemWeight() + "&length=" + inputObj.getItemLength() + "&width="
					+ inputObj.getItemWidth() + "&height=" + inputObj.getItemHeight();
		}
		StringEntity entity = new StringEntity(body, StandardCharsets.UTF_8);
		httpPost.setEntity(entity);

		try {
			HttpResponse response = httpClient.execute(httpPost);

			log.info(String.valueOf(response.getStatusLine().getStatusCode()));
			if (response.getStatusLine().getStatusCode() == 200) {

				String jsonResponse = EntityUtils.toString(response.getEntity());

				ObjectMapper mapper = new ObjectMapper();
				JsonNode root = mapper.readTree(jsonResponse);

				JsonNode dataNode = root.get("req").get("data");

				Double rate = dataNode.get("rate").asDouble();

				CourierRate courierRate = new CourierRate();
				courierRate.setCreatedDate(new Date());
				courierRate.setCourier(compObj.getCompanyName());
				courierRate.setRate(BigDecimal.valueOf(rate));
				courierRate.setCourierId(courierId);

				courierRateRepository.save(courierRate);
				log.info("Successfully insert to courier rate");

			} else {
				log.error("Error status code :" + response.getStatusLine().getStatusCode());
			}

		} catch (Exception ex) {
			log.error("Error ===> " + ex.getMessage());
		}

	}

	public Long insertTableCourier(Integer selectedType, InputObject inputObj) throws RunningNoEmptyException {

		Courier courierObj = new Courier();
		courierObj.setBatchNo(this.onGenerateBatchNo());
		courierObj.setCreatedDate(new Date());
		courierObj.setSelectedType(selectedType);
		courierObj.setSenderCountry(inputObj.getSenderCountry());
		courierObj.setSenderState(inputObj.getSenderState());
		courierObj.setSenderPoscode(inputObj.getSenderPoscode());
		courierObj.setReceiverCountry(inputObj.getReceiverCountry());
		courierObj.setReceiverState(inputObj.getReceiverState());
		courierObj.setReceiverPoscode(inputObj.getReceiverPoscode());
		courierObj.setItemHeight(inputObj.getItemHeight());
		courierObj.setItemLength(inputObj.getItemLength());
		courierObj.setItemWeight(inputObj.getItemWeight());
		courierObj.setItemWidth(inputObj.getItemWidth());

		Courier saveCourier = courierRepository.save(courierObj);
		log.info("Successfully Save the courier");
		return saveCourier.getCourierId();
	}

	public String onGenerateBatchNo() throws RunningNoEmptyException {

		String batchNo = "";

		RunningNo runObj = new RunningNo();
		runObj.setType("COUR");

		Optional<RunningNo> selectedRunningNo = runningNoRepository.findOne(Example.of(runObj));

		if (selectedRunningNo.isPresent()) {
			batchNo = selectedRunningNo.get().getType() + "/" + selectedRunningNo.get().getYears() + "/"
					+ String.format("%05d", selectedRunningNo.get().getRunningNo());
			Long incrementNo = selectedRunningNo.get().getRunningNo() + 1;

			selectedRunningNo.get().setRunningNo(incrementNo);
			log.info("Successfully Save - running_no : " + incrementNo);
			runningNoRepository.save(selectedRunningNo.get());

		} else {
			// throw
			log.error("Please check your Database in Table running_no");
			throw new RunningNoEmptyException("Please check your Database in Table running_no");

		}

		return batchNo;
	}

	public void onCheckingInputNull(Integer selectedType, InputObject inputObj) throws InputNullException {

		if (selectedType.equals(2)) {
			this.onCheckSameInput(inputObj);
		} else if (selectedType.equals(1)) {
			this.onCheckSameInput(inputObj);
			if (inputObj.getItemHeight() == null) {
				log.error("getItemHeight() cannot be null");
				throw new InputNullException("getItemHeight() cannot be null");
			}

			if (inputObj.getItemLength() == null) {
				log.error("getItemLength() cannot be null");
				throw new InputNullException("getItemLength() cannot be null");
			}

			if (inputObj.getItemWidth() == null) {
				log.error("getItemWidth() cannot be null");
				throw new InputNullException("getItemWidth() cannot be null");
			}
		} else {
//			log.error("1 - Parcel || 2 - Document");
			throw new InputNullException("1 - Parcel || 2 - Document");
		}
	}
	

	private void onCheckSameInput(InputObject inputObj) throws InputNullException {
		if (inputObj.getSenderCountry() == null) {
			log.error("getSenderCountry() cannot be null");
			throw new InputNullException("getSenderCountry() cannot be null");
		}

		if (inputObj.getSenderState() == null) {
			log.error("getSenderState() cannot be null");
			throw new InputNullException("getSenderState() cannot be null");
		}

		if (inputObj.getSenderPoscode() == null) {
			log.error("getSenderPoscode() cannot be null");
			throw new InputNullException("getSenderPoscode() cannot be null");
		}

		if (inputObj.getReceiverCountry() == null) {
			log.error("getReceiverCountry() cannot be null");
			throw new InputNullException("getReceiverCountry() cannot be null");
		}

		if (inputObj.getReceiverState() == null) {
			log.error("getReceiverState() cannot be null");
			throw new InputNullException("getReceiverState() cannot be null");
		}

		if (inputObj.getReceiverPoscode() == null) {
			log.error("getReceiverPoscode() cannot be null");
			throw new InputNullException("getReceiverPoscode() cannot be null");
		}

		if (inputObj.getItemWeight() == null) {
			log.error("getItemWeight() cannot be null");
			throw new InputNullException("getItemWeight() cannot be null");
		}
	}

	public void setHttpClient(HttpClient httpClient) {
		// TODO Auto-generated method stub
		
	}

}
