package com.tmservice.entity;


import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "courier_rate")
public class CourierRate {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long courierRateId;
	private String courier;
	private BigDecimal rate;
	private Date createdDate;
	private Long courierId;
	
	
}
