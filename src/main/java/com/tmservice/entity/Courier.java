package com.tmservice.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "courier")
public class Courier {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long courierId;
	private String batchNo;
	private Date createdDate;
	private boolean deleted = false;
	private Integer selectedType;
	private String senderCountry;
	private String senderState;
	private String senderPoscode;
	private String receiverCountry;
	private String receiverState;
	private String receiverPoscode;
	private BigDecimal itemLength;
	private BigDecimal itemWidth;
	private BigDecimal itemHeight;
	private BigDecimal itemWeight;
	
	@Transient
	List<CourierRate> courierRateList = new ArrayList<>();

}
