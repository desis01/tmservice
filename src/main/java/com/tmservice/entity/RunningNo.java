package com.tmservice.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity(name = "running_no")
public class RunningNo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long runningNoId;
	private Long runningNo;
	private String type;
	private String years;
	
	
	
}
