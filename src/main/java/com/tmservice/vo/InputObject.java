package com.tmservice.vo;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InputObject {
	private String senderCountry;
	private String senderState;
	private String senderPoscode;
	private String receiverCountry;
	private String receiverState;
	private String receiverPoscode;
	private BigDecimal itemLength;
	private BigDecimal itemWidth;
	private BigDecimal itemHeight;
	private BigDecimal itemWeight;
	
	
	

}
