package com.tmservice.vo;

import java.math.BigDecimal;

import com.tmservice.entity.CourierRate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CourierRateObject {
	
	private String courier;
	private BigDecimal rate;

	public CourierRateObject(CourierRate crObj) {
		this.courier = crObj.getCourier();
		this.rate = crObj.getRate();
	}
}
