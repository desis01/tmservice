package com.tmservice.vo;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OutputObject {
	private List<CourierRateObject> data;
}
