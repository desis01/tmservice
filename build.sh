action=$1
name=${2:=testservice}
version=${3:=latest}
registry=${4:=registry.gitlab.com}
username=${5:=ahahah}
password=${6:=hahaha}

if [ $version = 'auto-version' ]
then
    version=`cat src/main/resources/VERSION`
    echo "version: $version"
fi

imageName=$name:latest
registryImageNameLatest=$registry/$name:latest
registryImageNameVersion=$registry/$name:$version

echo "Maven package starts"
mvn clean package

docker build -f Dockerfile . -t $imageName

docker tag $imageName $registryImageNameLatest
docker tag $imageName $registryImageNameVersion

echo "Login into GitLab Registry."; docker login -u=$username -p=$password https://registry.gitlab.com/v2/

docker push $registryImageNameLatest
docker push $registryImageNameVersion

docker logout https://registry.gitlab.com/v2/
